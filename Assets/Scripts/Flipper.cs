﻿using UnityEngine;
using System.Collections;

public class Flipper : MonoBehaviour
{
	public float addAngularVelocity;
	public KeyCode key;

	private void Awake()
	{
		GetComponent<Rigidbody>().maxAngularVelocity = 10000f;
    }

	/// <summary>
	/// 更新処理
	/// </summary>
	void Update()
	{
		if (Input.GetKey(key))
		{
			GetComponent<Rigidbody>().angularVelocity += new Vector3(0f, addAngularVelocity);
		}
		else
		{
			GetComponent<Rigidbody>().angularVelocity -= new Vector3(0f, addAngularVelocity);
		}
	}
}
